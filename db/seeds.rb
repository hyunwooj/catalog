programs = {}
["Computer Science", "Electrical and Computer Engineering", "Humanities"].each do |name|
  programs[name] = Program.create(name: name)
end

operators = {}
["and", "or", "min_grade"].each do |name|
  operators[name] = Operator.create(name: name)
end

grades = {}
["A+", "A", "A-", "B+", "B", "B-", "C+", "C", "C-", "D+", "D", "D-", "F"].each do |name|
  grades[name] = Grade.create(name: name)
end

courses = {}
["CS 131L", "CS 132L", "CS 151L", "CS 152L", "CS 251L", "CS 241L", "CS 259L", "CS 261", "CS 361L"].each do |name|
  courses[name] = Course.create(name: name, description: "A #{name.sub(/\s.*/, '')} course", program_id: Program.find_by_name('Computer Science').id)
end

courses["CS 132L"].prerequisite = Prerequisite.create(
  operator: operators["min_grade"].id,
  operandA: courses["CS 131L"].id,
  operandB: grades["F"].id
)
courses["CS 132L"].save

courses["CS 251L"].prerequisite = Prerequisite.create(
  operator: operators["or"].id,
  operandA: Prerequisite.create(
    operator: operators["min_grade"].id,
    operandA: courses["CS 151L"].id,
    operandB: grades["F"].id).id,
  operandB: Prerequisite.create(
    operator: operators["min_grade"].id,
    operandA: courses["CS 152L"].id,
    operandB: grades["F"].id).id
)
courses["CS 251L"].save

courses["CS 241L"].prerequisite = Prerequisite.create(
  operator: operators["or"].id,
  operandA: Prerequisite.create(
    operator: operators["or"].id,
    operandA: Prerequisite.create(
      operator: operators["min_grade"].id,
      operandA: courses["CS 151L"].id,
      operandB: grades["F"].id).id,
    operandB: Prerequisite.create(
      operator: operators["min_grade"].id,
      operandA: courses["CS 152L"].id,
      operandB: grades["F"].id).id
    ).id,
  operandB: Prerequisite.create(
    operator: operators["min_grade"].id,
    operandA: courses["CS 259L"].id,
    operandB: grades["F"].id).id
)
courses["CS 241L"].save

courses["CS 361L"].prerequisite = Prerequisite.create(
  operator: operators["and"].id,
  operandA: Prerequisite.create(
    operator: operators["min_grade"].id,
    operandA: courses["CS 261"].id,
    operandB: grades["F"].id).id,
  operandB: Prerequisite.create(
    operator: operators["min_grade"].id,
    operandA: courses["CS 241L"].id,
    operandB: grades["F"].id).id
)
courses["CS 361L"].save

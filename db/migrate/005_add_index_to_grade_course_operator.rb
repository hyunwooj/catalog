class AddIndexToGradeCourseOperator < ActiveRecord::Migration
  def change
    add_index :grades, :name, unique: true
    add_index :courses, :name, unique: true
    add_index :operators, :name, unique: true
  end
end

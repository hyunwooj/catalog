class AddCourseRefToPrerequisites < ActiveRecord::Migration
  def change
    add_reference :prerequisites, :course, index: {:unique=>true}, foreign_key: true
  end
end

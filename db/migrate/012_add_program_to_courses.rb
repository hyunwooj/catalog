class AddProgramToCourses < ActiveRecord::Migration
  def change
    add_reference :courses, :program, index: true, foreign_key: true
  end
end

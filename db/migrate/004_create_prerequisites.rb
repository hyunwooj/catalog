class CreatePrerequisites < ActiveRecord::Migration
  def change
    create_table :prerequisites do |t|
      t.integer :operator
      t.integer :operandA
      t.integer :operandB

      t.timestamps null: false
    end
  end
end

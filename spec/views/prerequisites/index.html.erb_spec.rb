require 'rails_helper'

RSpec.describe "prerequisites/index", type: :view do
  before(:each) do
    assign(:prerequisites, [
      Prerequisite.create!(
        :operator => 1,
        :operandA => 2,
        :operandB => 3
      ),
      Prerequisite.create!(
        :operator => 1,
        :operandA => 2,
        :operandB => 3
      )
    ])
  end

  it "renders a list of prerequisites" do
    render
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
  end
end

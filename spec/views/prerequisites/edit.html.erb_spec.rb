require 'rails_helper'

RSpec.describe "prerequisites/edit", type: :view do
  before(:each) do
    @prerequisite = assign(:prerequisite, Prerequisite.create!(
      :operator => 1,
      :operandA => 1,
      :operandB => 1
    ))
  end

  it "renders the edit prerequisite form" do
    render

    assert_select "form[action=?][method=?]", prerequisite_path(@prerequisite), "post" do

      assert_select "input#prerequisite_operator[name=?]", "prerequisite[operator]"

      assert_select "input#prerequisite_operandA[name=?]", "prerequisite[operandA]"

      assert_select "input#prerequisite_operandB[name=?]", "prerequisite[operandB]"
    end
  end
end

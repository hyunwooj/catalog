Prerequisites
=============
- Ruby
- PostgreSQL
- Bash

Installation
============
All the following commands are written in *Bash*.

1. Clone the project repository

        $ git clone REPO_URL

1. Install gems

        $ bundle install

1. Install pre-commit hook

        $ ln -s "$(pwd)/ci/pre-commit" .git/hooks/

//= require jquery
//= require jquery.turbolinks
//= require jquery_ujs
//= require bootstrap-sprockets
//= require turbolinks
//= require_tree .


var onResize = function() {
  // apply dynamic padding at the top of the body according to the fixed navbar height
  $("body").css("padding-top", $(".nav").height());
};

// attach the function to the window resize event
$(window).resize(onResize);

// call it also when the page is ready after load or reload
$(function() {
  onResize();
  cal();
});

var cal = function() {
  $("#courses .pagination a").on("click", function() {
    $.getScript(this.href);
    return false;
  });
  $("#courses_search input").keyup(function() {
    $.get($("#courses_search").attr("action"), $("#courses_search").serialize(), null, "script");
    return false;
  });
};

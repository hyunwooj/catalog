require 'tree'

class Prerequisite < ActiveRecord::Base
  belongs_to :course

  def print_tree(t)
    if t.has_children?
      if t.content.operator == Operator.where(name: "and").first.id
        op = " and "
      else
        op = " or "
      end
      return "(" + self.print_tree(t['left'].content) + op + self.print_tree(t['right'].content) + ")"
    else
      p = t.content
      c = Course.find(p.operandA).name
      g = Grade.find(p.operandB).name
      return "(" + c + " >= " + g + ")"
    end
  end

  def descendant_tree
    if has_children?
      l, r = children
      node = Tree::TreeNode.new(id.to_s, self)
      node << Tree::TreeNode.new("left", l.descendant_tree)
      node << Tree::TreeNode.new("right", r.descendant_tree)
      return node
    else
      node = Tree::TreeNode.new(id.to_s, self)
      return node
    end
  end

  def children
    return [Prerequisite.find(operandA), Prerequisite.find(operandB)]
  end

  def has_children?
    # TODO: don't have to find 'and' and 'or' operator every time this method is called.
    # What a poor ORM ActiveRecord... does not it support 'or' method?
    ids = Operator.where("name = 'and' or name = 'or'").all.map { |op| op.id }
    return ids.include?(operator)
  end
end

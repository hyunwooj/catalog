class Program < ActiveRecord::Base

  has_many :courses

  def self.search(search)
    if search
      where('name LIKE ?', "%#{search}%")
    else
      all
    end
  end

end

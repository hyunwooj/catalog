Rails.application.routes.draw do

  resources :programs
  root                              'home#index'

  get   'about'                 =>  'home#about'
  get   'faq'                   =>  'home#faq'
  get   'contact'               =>  'contacts#new'
  get   'old_contact'           =>  'home#contact'

  resources :prerequisites
  resources :courses
  resources :contacts

end
